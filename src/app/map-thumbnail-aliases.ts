const aliases = {
  '^cp_prolands.*$': 'cp_badlands',
  '^cp_reckoner.*$': 'cp_reckoner',
  '^koth_product.*$': 'koth_product',
}

export default function getMapAlias(mapName: string) : string {
  for (let key in aliases) {
    if (new RegExp(key).test(mapName)) {
      return aliases[key];
    }
  }

  return mapName;
}
