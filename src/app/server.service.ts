import { Injectable } from '@angular/core';
import { Http } from '@angular/http';

import 'rxjs/add/operator/toPromise';

import { Server } from './server';


@Injectable()
export class ServerService {
  private serversUrl = 'http://api.melkor.tf/servers';

  constructor(private http: Http) { }

  getServers(): Promise<Server[]> {
    return this.http.get(this.serversUrl)
      .toPromise()
      .then(response => response.json() as Server[]);
  }
}
