import { TestBed, inject } from '@angular/core/testing';

import { MapThumbnailService } from './map-thumbnail.service';

describe('MapThumbnailService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [MapThumbnailService]
    });
  });

  it('should be created', inject([MapThumbnailService], (service: MapThumbnailService) => {
    expect(service).toBeTruthy();
  }));
});
