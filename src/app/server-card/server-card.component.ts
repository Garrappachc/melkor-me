import { Component, Input, OnInit } from '@angular/core';

import { MapThumbnailService } from '../map-thumbnail.service'
import { Server } from '../server';

@Component({
  selector: 'app-server-card',
  templateUrl: './server-card.component.html',
  styleUrls: ['./server-card.component.css'],
  providers: [MapThumbnailService]
})
export class ServerCardComponent implements OnInit {

  @Input() server: Server;
  mapImg: string;

  constructor(private mapThumbnailService: MapThumbnailService) { }

  ngOnInit() {
    this.getThumbnail();
  }

  getThumbnail(): void {
    this.mapThumbnailService.getMapThumbnail(this.server.map).then(img => this.mapImg = img);
  }

}
