import { Injectable } from '@angular/core';
import { Http, Response } from '@angular/http';

import getMapAlias from './map-thumbnail-aliases';

@Injectable()
export class MapThumbnailService {

  constructor(private http: Http) { }

  getMapThumbnail(map: string): Promise<string> {
    return new Promise((resolve, reject) => {
      const thumbnailName = '/assets/' + getMapAlias(map) + '.jpg';

      this.imageExists(thumbnailName).then((result) => {
        resolve(result ? thumbnailName : '/assets/cp_badlands.jpg');
      });
    });
  }

  private imageExists(url: string): Promise<boolean> {
    return new Promise((resolve, reject) => {
      let response: Response;

      this.http.get(url)
        .subscribe(
          (res) => response = res,
          () => resolve(false),
          () => resolve(response.ok)
        );
    });
  }

}
