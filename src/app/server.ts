export class Server {
    name: string;
    online: boolean = true;
    address: string;
    port: number;
    map: string
    players: number;
    maxplayers: number;
}