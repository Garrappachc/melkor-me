import { Component } from '@angular/core';
import { OnInit } from '@angular/core';

import { Server } from './server';
import { ServerService } from './server.service';

@Component({
    selector: 'app-root',
    templateUrl: './app.component.html',
    styleUrls: ['./app.component.css'],
    providers: [ServerService]
})
export class AppComponent implements OnInit {
    title = 'melkor.tf servers';
    servers: Server[];

    constructor(private serverService: ServerService) { }

    ngOnInit(): void {
        this.getServers();
    }

    getServers(): void {
        this.serverService.getServers().then(servers => this.servers = servers);
    }
}

