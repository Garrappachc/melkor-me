import { MelkorMePage } from './app.po';

describe('melkor-me App', () => {
  let page: MelkorMePage;

  beforeEach(() => {
    page = new MelkorMePage();
  });

  it('should display message saying app works', () => {
    page.navigateTo();
    expect(page.getParagraphText()).toEqual('app works!');
  });
});
